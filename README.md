# Secure boot and software integrity

It is a proof of concept to generate encrypted signatures of a binary.
Addiltionally we are encrypting the binary too.

## MD5 generation

To generate MD5 signature from a binary, it uses the command:
```
md5sum
```
Note: it can be improved with SHA-256

## AES GCM Encryption

To encrypt files we will used the algorithm AES GCM.
The implementation uses the python library: pycryptodome 
And onboard there ir a C implementation.

## How to execute

In the script ./scripts/sign_binary.sh you can indicate:
* binary name
* package=true or false (It indicates if we need to package the encrypted binary with TAS package tool)

Then it generates md5 signature from the binary content and then encrypt binary and signature.
Finally it uploads both items to an artifactory. See ARTIFACTORY_URL variable.

To execute:
``` 
./scripts/sign_binary.sh 
```

To verify, execute (it is a simulation of onbaord software functionality):
``` 
./scripts/verify_binary.sh 
```

## Tests

### MD5 tests
To test MD5 we can execute the test:
```
./scripts/md5test.sh 
```
The output expected is:
```
d41d8cd98f00b204e9800998ecf8427e  -
d41d8cd98f00b204e9800998ecf8427e
0cc175b9c0f1b6a831c399e269772661  -
0cc175b9c0f1b6a831c399e269772661
900150983cd24fb0d6963f7d28e17f72  -
900150983cd24fb0d6963f7d28e17f72
f96b697d7cb7938d525a2f31aaf161d0  -
f96b697d7cb7938d525a2f31aaf161d0
403473fa0d8a951c9571f7b211c7258e  -
403473fa0d8a951c9571f7b211c7258e
f67e503f52ed8d19ab0b8d64e1b68c3d  -
f67e503f52ed8d19ab0b8d64e1b68c3d
```

### AES GCM tests
To test AES GCM algorithm we can execute:
```
python3 python/pyaesgcm_test.py
```

The output expected is:
```
****************** Test 1 ******************
Message encrypt:  b'Hola!'
iv value obtained:  a9df343ee1cc1f6decb516b88fb22ba6
Message decrypt:  b'Hola!'
****************** Test 2 ******************
Message encrypt:  b'Otro mensaje'
iv value obtained:  5eeb5bf7d2968905fa562903bcd8ce59
Message decrypt:  b'Otro mensaje'
****************** Test 3 ******************
Message encrypt:  d9313225f88406e5a55909c5aff5269a86a7a9531534f7da2e4c303d8a318a721c3c0c95956809532fcf0e2449a6b525b16aedf5aa0de657ba637b391aafd255
Message decrypt:  d9313225f88406e5a55909c5aff5269a86a7a9531534f7da2e4c303d8a318a721c3c0c95956809532fcf0e2449a6b525b16aedf5aa0de657ba637b391aafd255
cipher text value obtained:  42831ec2217774244b7221b784d0d49ce3aa212f2c02a4e035c17e2329aca12e21d514b25466931c7d8f6a5aac84aa051ba30b396a0aac973d58e091473f5985
cipher text value expected:  42831ec2217774244b7221b784d0d49ce3aa212f2c02a4e035c17e2329aca12e21d514b25466931c7d8f6a5aac84aa051ba30b396a0aac973d58e091473f5985
iv value obtained:  cafebabefacedbaddecaf888
iv value expected:  cafebabefacedbaddecaf888
tag value obtained:  4d5c2af327cd64a62cf35abd2ba6fab4
tag value expected:  4d5c2af327cd64a62cf35abd2ba6fab4
```

## Annex: Other algorithms to encrypt

### AES 256
For this algorithm we can use symmetric or asymmetric encryption.

#### Symmetric option
```
echo 'encrypting with gpg AES 256 symmetric'
gpg --verbose --symmetric --batch --passphrase $SECRET_KEY --cipher-algo AES256 hellomake.md5
```
```
echo 'decrypting with gpg AES 256 symmetric'
gpg --batch --passphrase "$SECRET_KEY" -o hellomake.md5.download -d hellomake.md5.gpg
```

#### Asymmetric option (we need to generate pair keys before with gpg)
```
# asymmetric option with publick key to encrypt and private key to decrypt
echo 'encrypting with gpg AES 256 asymmetric'
gpg -e -r foo --verbose hellomake.md5
```

```
echo 'decrypting with gpg AES 256 asymmetric'
gpg -r foo -o hellomake.md5.download -d hellomake.md5.gpg
```

## Generation of pair keys with GPG
```
# echo '******************************************'
# echo 'Generating keys'
# gpg-connect-agent /bye
# export GNUPGHOME="$(mktemp -d)"
# export GPG_TTY="$(tty)"
# gpg --batch --generate-key foo
# gpg --list-secret-keys
# gpg --list-keys
# echo '******************************************'
```
