export COVERITY_HOME=/home/sebas/cov-analysis-linux64-2022.6.1
export PATH="$PATH:$COVERITY_HOME/bin"
rm hellomake
cov-configure --gcc
cov-build --dir .cov make
cov-analyze --dir .cov --all --distrust-all --enable-constraint-fpp --enable-single-virtual --security -aggressiveness-level high --coding-standard-config ./tas-cyber-c-v1.config
cov-format-errors --dir .cov --json-output-v8 report-coverity.json
