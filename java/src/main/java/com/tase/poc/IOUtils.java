package com.tase.poc;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class IOUtils {

    public static String readFromInputStream(String fileName) throws IOException {
        FileInputStream inputStream = new FileInputStream(fileName);
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }


    public static void dumpToFile(byte[] content, String fileName) throws IOException {
        Path path = Paths.get(fileName);
        Files.write(path, content);
    }
}
