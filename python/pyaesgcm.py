from Crypto.Cipher import AES
import sys
import os

SIZE_IV = 16
SIZE_TAG = 16


def encrypt_AES_GCM(message, secret_key):
    aes_cipher = AES.new(secret_key, AES.MODE_GCM)
    ciphertext, tag = aes_cipher.encrypt_and_digest(message)
    return ciphertext, aes_cipher.nonce, tag


def encrypt_AES_GCM_with_iv(message, secret_key, initial_vector):
    aes_cipher = AES.new(secret_key, AES.MODE_GCM, initial_vector)
    ciphertext, tag = aes_cipher.encrypt_and_digest(message)
    return ciphertext, aes_cipher.nonce, tag


def decrypt_AES_GCM(encryptedMsg, secretKey):
    (ciphertext, initial_vector, tag) = encryptedMsg
    aes_cipher = AES.new(secretKey, AES.MODE_GCM, initial_vector)
    plaintext = aes_cipher.decrypt_and_verify(ciphertext, tag)
    return plaintext


def process(path_file, secret_key, initial_vector, user_option):

    file_name = os.path.basename(path_file)
    if user_option == "1":
        message = read_file(path_file)
        encrypted_msg = encrypt_AES_GCM(message, secret_key)
        dump_to_file_encrypted(encrypted_msg, file_name)
    elif user_option == "2":
        encrypted_msg_read = read_from_file(file_name)
        decrypted_msg = decrypt_AES_GCM(encrypted_msg_read, secret_key)
        dump_to_file(decrypted_msg, file_name + ".decrypted")
    elif user_option == "3":
        message = read_file(path_file)
        encrypted_msg = encrypt_AES_GCM_with_iv(message, secret_key, initial_vector)
        dump_to_file_encrypted(encrypted_msg, file_name)
    else:
        print('Please introduce a valid option')


def read_file(path):
    file_to_encrypt = open(path, 'rb')
    message = bytes(file_to_encrypt.read())
    file_to_encrypt.close()
    return message


def dump_to_file(message, file_name):
    file_out = open(file_name, "wb")
    file_out.write(message)
    file_out.close()


def dump_to_file_encrypted(encrypted_msg, file_name):
    (ciphertext, nonce, authTag) = encrypted_msg

    file_out = open(file_name + ".encrypted", "wb")
    [file_out.write(x) for x in (nonce, authTag, ciphertext)]
    file_out.close()


def read_from_file(file_name):
    file_in = open(file_name + ".encrypted", "rb")
    initial_vector, tag, ciphertext = [file_in.read(x) for x in (SIZE_IV, SIZE_TAG, -1)]
    file_in.close()

    encrypted_msg_read = (ciphertext, initial_vector, tag)
    return encrypted_msg_read


if __name__ == '__main__':
    if len(sys.argv) == 5:
        iv = bytes(sys.argv[3], 'utf-8')
        option = sys.argv[4]
    elif len(sys.argv) == 4:
        option = sys.argv[3]
    else:
        print('first argument should be the file to encrypt')
        print('second argument should be the secret key')
        print('third argument should be the initial vector (optionally)')
        print('fourth argument should be the option: 1 for encrypt, 2 for decrypt, 3 for encrypt with iv, 4 for decrypt with IV')
        sys.exit()
    file = sys.argv[1]
    key = bytes(sys.argv[2], 'utf-8')
    key = b'\xfe\xff\xe9\x92\x86\x65\x73\x1c\x6d\x6a\x8f\x94\x67\x30\x83\x08'
    iv = b'\xca\xfe\xba\xbe\xfa\xce\xdb\xad\xde\xca\xf8\x88'
    process(file, key, iv, option)
