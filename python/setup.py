#!/usr/bin/env python
from setuptools import setup

version = 1.0

setup(
    name='pyaesgcm',
    version=version,
    packages=[
    ],
    author='Thales Alenia Space',
    url='www.thalesgroup.com',
    author_email='sathound@thales.invalid',
    install_requires=[
        'pycryptodome==3.14.1'
    ]
)
