echo '******************************************'
echo 'Integrity and intellectual property protection of binary: hellomake'
echo '******************************************'

echo '******************************************'
echo 'Generating hash (128 bits) from binary'
md5sum -b hellomake > hellomake.md5
head -c 32 hellomake.md5 > md5 # we get first 128 bits = 32 bytes
echo '******************************************'

echo '******************************************'
echo 'Encrypting with python implementation of AES GCM'
echo '******************************************'
pip install pycryptodome
python3 ./python/pyaesgcm.py hellomake $SECRET_KEY 1
python3 ./python/pyaesgcm.py md5 $SECRET_KEY 1 # optionally we can select different keys
mkdir upload
mv hellomake.encrypted ./upload/hellomake.encrypted
mv md5.encrypted ./upload/md5.encrypted
rm md5
echo '******************************************'

mkdir logs
echo '******************************************'
echo 'Upload to artifactory: md5 and binary encrypted '
echo '******************************************'
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/hellomake.encrypted \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/hellomake.encrypted" > logs/out1.txt

curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/md5.encrypted \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/md5.encrypted" > logs/out3.txt
