echo '******************************************'
echo 'Signing binary: hellomake'
echo '******************************************'

echo '******************************************'
echo 'Generating md5 hash from binary: hellomake.md5'
md5sum -b hellomake > hellomake.md5
head -c 32 hellomake.md5 > md5_32_bits
cat hellomake md5_32_bits > hellomake_md5_embedded
echo '******************************************'

echo '******************************************'
echo 'Encrypting with python implementation of AES GCM: generating hellomake_md5_embedded.encrypted'
echo '******************************************'
pip install pycryptodome
python3 ./python/pyaesgcm.py hellomake_md5_embedded $SECRET_KEY 1
mkdir upload
mv hellomake_md5_embedded.encrypted ./upload/hellomake_md5_embedded.encrypted
#mv hellomake.md5 ./upload/hellomake.md5
#cp hellomake ./upload/hellomake
echo '******************************************'

mkdir logs
echo '******************************************'
echo 'Upload to artifactory: hellomake_md5_embedded(binary) '
echo '******************************************'
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/hellomake_md5_embedded.encrypted \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/hellomake_md5_embedded.encrypted" > logs/out1.txt

# curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
#        -X PUT \
#        -T ./upload/hellomake.md5.encrypted \
#        "$ARTIFACTORY_URL/artifactory/example-repo-local/hellomake.md5.encrypted" > logs/out3.txt
