# remove crc final (4 bytes) if needed
newfsize=$(($(stat -c '%s' bsw_cfg.img) - 4)) 
head -c $newfsize bsw_cfg.img > bsw_cfg_no_last_crc.img
## remove header (16 bytes)
newfsize=$(($(stat -c '%s' bsw_cfg_no_last_crc.img) - 16)) 
tail -c $newfsize bsw_cfg_no_last_crc.img > bsw_cfg_content.img