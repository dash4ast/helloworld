echo '******************************************'
echo 'Download from artifactory: binary and md5 encypted'
echo '******************************************'

rm -rf download
mkdir download
cd download

curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/hellomake.encrypted"
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/md5.encrypted"

echo '***********************************'
echo 'Decrypting with PYTHON IMPL AESGCM'
python3 ../python/pyaesgcm.py hellomake $SECRET_KEY 2
python3 ../python/pyaesgcm.py md5 $SECRET_KEY 2
echo '***********************************'

echo '***********************************'
echo 'Calculating md5 binary download'
md5sum -b hellomake.decrypted > hellomake.md5.decrypted
head -c 32 hellomake.md5.decrypted > md5_binary_decrypted
echo '***********************************'

echo '***********************************'
echo 'Compare both md5'
echo '***********************************'
cat md5.decrypted
echo '***********************************'
cat md5_binary_decrypted
echo '***********************************'

