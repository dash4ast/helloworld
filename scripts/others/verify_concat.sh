echo '******************************************'
echo 'Download from artifactory: binary (hellomake_md5_embedded.encrypted)'
echo '******************************************'

mkdir download
cd download

curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/hellomake_md5_embedded.encrypted"
# curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
#        -O "$ARTIFACTORY_URL/artifactory/example-repo-local/hellomake"

echo '***********************************'
echo 'Decrypting with PYTHON IMPL AESGCM: generating hellomake_md5_embedded.decrypted'
python3 ../python/pyaesgcm.py hellomake_md5_embedded $SECRET_KEY 2
echo '***********************************'

echo '***********************************'
echo 'Calculate md5 binary download: generating hellomake.md5.calculated'
tail -c 32 hellomake_md5_embedded.decrypted > hellomake.md5.decrypted
truncate -s -32 hellomake_md5_embedded.decrypted > hellomake2
md5sum -b hellomake2 > hellomake.md5.calculated
head -c 32 hellomake.md5.calculated > md5_32_bits_calculated
echo '***********************************'

echo '***********************************'
echo 'Compare hellomake.md5.calculated with hellomake.md5.decrypted'
#diff hellomake.md5.decrypted hellomake.md5.calculated
echo '***********************************'
cat hellomake.md5.decrypted
echo '***********************************'
cat md5_32_bits_calculated
echo '***********************************'
#md5sum -c hellomake.md5.decrypted
echo '***********************************'
