export SECRET_KEY=12345678123456781234567812345678
export ARTIFACTORY_URL=https://sebas.jfrog.io
export API_KEY_ARTIFACTORY=AKCp8mZ8SgutaKrtc6LEXJHo61jBrtmQ4n6ZsuqEW3Vz4a71hoqaNGMjTFy8MPP5joZgDoLnR

binary_name=bsw_cfg.img
package=true
#binary_name=testcase
#package=false

cp binaries_to_test/$binary_name .
echo '*********************************************************'
echo 'Integrity and intellectual property protection of binary:' $binary_name 
echo '*********************************************************'

echo '******************************************'
echo 'Generating hash (128 bits) from binary content'
content=$(tr -d '\0' <$binary_name)
echo -n $content | md5sum -b > $binary_name.filename.sign
head -c 32 $binary_name.filename.sign > $binary_name.sign # we get first 128 bits = 16 bytes. We put 32 because 32 characters in hex are 16 bytes.
echo '******************************************'

echo '******************************************'
echo 'Encrypting with python implementation of AES GCM'
echo '******************************************'
#pip install pycryptodome
python3 ./python/pyaesgcm.py $binary_name $SECRET_KEY 1 # Option 1 means Encrypt without initial vector
python3 ./python/pyaesgcm.py $binary_name.sign $SECRET_KEY 1 # optionally we can select different keys

rm -rf upload
mkdir upload
mv $binary_name.encrypted ./upload/$binary_name.encrypted
mv $binary_name.sign.encrypted ./upload/$binary_name.sign.encrypted
rm $binary_name.sign
rm $binary_name.filename.sign
echo '******************************************'

if [ "$package" = true ] ; then
    echo '******************************************'
    echo 'Package binary and md5 (add CRC and headers)'
    echo '******************************************'
    /home/sebas/tas/source/sign/obsw_tools/jbbuilder/jbb.sh -i conf/bsw_cfg.xml -o upload/bsw_cfg.bin
    /home/sebas/tas/source/sign/obsw_tools/jbbuilder/jbb.sh -i conf/bsw_cfg_md5.xml -o upload/bsw_cfg_sign.bin
fi

rm -rf logs
mkdir logs

echo '******************************************'
echo 'Upload to artifactory: md5 and binary encrypted '
echo '******************************************'
echo "Connecting to: $ARTIFACTORY_URL...."
if [ "$package" = false ] ; then
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/$binary_name.encrypted \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/$binary_name.encrypted" > logs/out3.txt

curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/$binary_name.md5.encrypted \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/$binary_name.sign.encrypted" > logs/out3.txt
fi
if [ "$package" = true ] ; then
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/bsw_cfg.bin \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/bsw_cfg.bin" > logs/out3.txt

curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -X PUT \
       -T ./upload/bsw_cfg_sign.bin \
       "$ARTIFACTORY_URL/artifactory/example-repo-local/bsw_cfg_sign.bin" > logs/out3.txt

fi
echo "Successfully updated to artifactory..."
rm $binary_name 