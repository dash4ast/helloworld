export SECRET_KEY=12345678123456781234567812345678
export ARTIFACTORY_URL=https://sebas.jfrog.io
export API_KEY_ARTIFACTORY=AKCp8mZ8SgutaKrtc6LEXJHo61jBrtmQ4n6ZsuqEW3Vz4a71hoqaNGMjTFy8MPP5joZgDoLnR

binary_name=bsw_cfg.img
package=true
#binary_name=testcase
#package=false

echo '***************************************************'
echo 'Download from artifactory: content and md5 encrypted for binary:' $binary_name
echo '***************************************************'

if [ "$package" = false ] ; then
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/$binary_name.encrypted"
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/$binary_name.sign.encrypted"
fi
if [ "$package" = true ] ; then
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/bsw_cfg.bin"
curl -sSf -H "X-JFrog-Art-Api:$API_KEY_ARTIFACTORY" \
       -O "$ARTIFACTORY_URL/artifactory/example-repo-local/bsw_cfg_sign.bin"


# remove crc final (2 bytes) if needed
newfsize=$(($(stat -c '%s' bsw_cfg.bin) - 2)) 
head -c $newfsize bsw_cfg.bin > bsw_cfg_no_last_crc.tmp
## remove header (16 bytes)
newfsize=$(($(stat -c '%s' bsw_cfg_no_last_crc.tmp) - 16)) 
tail -c $newfsize bsw_cfg_no_last_crc.tmp > bsw_cfg_content.tmp
mv bsw_cfg_content.tmp $binary_name.encrypted

# remove crc final (2 bytes) if needed
newfsize=$(($(stat -c '%s' bsw_cfg_sign.bin) - 2)) 
head -c $newfsize bsw_cfg_sign.bin > bsw_sign_cfg_no_last_crc.tmp
## remove header (16 bytes)
newfsize=$(($(stat -c '%s' bsw_sign_cfg_no_last_crc.tmp) - 16)) 
tail -c $newfsize bsw_sign_cfg_no_last_crc.tmp > bsw_sign_cfg_content.tmp
mv bsw_sign_cfg_content.tmp $binary_name.sign.encrypted
fi

echo '***********************************'
echo 'Decrypting with PYTHON IMPL AESGCM'
python3 python/pyaesgcm.py $binary_name $SECRET_KEY 2 # Option 2 means Decrypt
python3 python/pyaesgcm.py $binary_name.sign $SECRET_KEY 2 # Option 2 means Decrypt
echo '***********************************'

echo '***********************************'
echo 'Calculating md5 binary download'
content=$(tr -d '\0' <$binary_name.decrypted)
echo -n $content | md5sum -b > $binary_name.sign.decrypted.withfilename.tmp
head -c 32 $binary_name.sign.decrypted > sign_binary_decrypted.tmp
echo '***********************************'

echo '***********************************'
echo 'Compare both md5'
echo '***********************************'
head -c 32 $binary_name.sign.decrypted.withfilename.tmp > $binary_name.sign.decrypted
cat $binary_name.sign.decrypted
echo ' '
echo '***********************************'
cat sign_binary_decrypted.tmp
echo ' '
echo '***********************************'

rm -rf tmp
mkdir tmp
mv $binary_name.decrypted tmp/$binary_name.decrypted
mv $binary_name.encrypted tmp/$binary_name.encrypted
mv $binary_name.sign.encrypted tmp/$binary_name.sign.encrypted
mv $binary_name.sign.decrypted tmp/$binary_name.sign.decrypted
if [ "$package" = true ] ; then
mv *.bin tmp/
fi
rm *.tmp
